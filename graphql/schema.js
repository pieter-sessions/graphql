import { GraphQLSchema, GraphQLObjectType } from "graphql";

import rideQueries from "./rides/queries";
import rideMutations from "./rides/mutations";
import rideSubscriptions from "./rides/subscriptions";

export default new GraphQLSchema({
  query: new GraphQLObjectType({
    name: "Query",
    fields: {
      ...rideQueries
    }
  }),
  mutation: new GraphQLObjectType({
    name: "Mutation",
    fields: {
      ...rideMutations
    }
  }),
  subscription: new GraphQLObjectType({
    name: "Subscription",
    fields: {
      ...rideSubscriptions
    }
  })
});
