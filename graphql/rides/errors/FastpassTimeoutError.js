import { createError } from 'apollo-errors';

export const FastpassTimeoutError = createError('FastpassTimeoutError', {
  message: 'The fastpass system could not be reached'
});