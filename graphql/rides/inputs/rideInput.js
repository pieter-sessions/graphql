import { GraphQLInputObjectType } from "graphql";
import RidenameScalar from "../types/RidenameScalar";
import RideCategoryEnum from "../types/RideCategoryEnum";

export default new GraphQLInputObjectType({
  name: "rideInput",
  fields: () => ({
    name: { type: RidenameScalar },
    category: { type: RideCategoryEnum }
  })
});
