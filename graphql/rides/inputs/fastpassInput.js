import { GraphQLInputObjectType, GraphQLString, GraphQLList } from "graphql";

export default new GraphQLInputObjectType({
  name: "fastpassInput",
  fields: () => ({
    rideId: { type: GraphQLString },
    timeslot: { type: GraphQLString }
  })
});
