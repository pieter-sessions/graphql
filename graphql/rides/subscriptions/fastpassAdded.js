import Fastpass from "../types/fastpass";

export default {
  type: Fastpass,
  async subscribe(source, args, ctx) {
    return ctx.pubSub.asyncIterator(["NEW_FASTPASS"]);
  },
  resolve: payload => {
    return { rideId: payload.rideId, timeslot: payload.timeslot };
  }
};
