import DataLoader from "dataloader";
import Fastpass from "../../../model/Fastpass";

async function fetchFastpasses(keys) {
  const passes = await Fastpass.find({ rideId: { $in: keys } });
  return keys.map(key => passes.filter(pass => pass.rideId === key));
}

export default {
  fastpassLoader() {
    return new DataLoader(keys => fetchFastpasses(keys), {
      cacheKeyFn: key => key.toString()
    });
  }
};
