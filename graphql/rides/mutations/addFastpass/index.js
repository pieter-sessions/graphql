import { GraphQLList } from "graphql";

import Fastpass from "../../types/fastpass";
import FastpassModel from "../../../../model/Fastpass";
import fastpassInput from "../../inputs/fastpassInput";

export default {
  type: new GraphQLList(Fastpass),
  args: {
    fastpass: {
      type: fastpassInput
    }
  },
  async resolve(source, args, ctx) {
    const fastPass = new FastpassModel({
      rideId: args.fastpass.rideId,
      timeslot: args.fastpass.timeslot
    });
    await fastPass.save();

    ctx.pubSub.publish("NEW_FASTPASS", fastPass);
    return FastpassModel.find({ rideId: args.fastpass.rideId });
  }
};
