import { GraphQLList } from "graphql";
import uuid from "uuid/v4";

import Ride from "../../types/ride";
import RideModel from "../../../../model/Ride";
import RideInput from "../../inputs/rideInput";

export default {
  type: new GraphQLList(Ride),
  args: {
    ride: {
      type: RideInput
    }
  },
  async resolve(source, args) {
    await RideModel.insertMany({
      id: uuid(),
      name: args.ride.name
    });
    return RideModel.find();
  }
};
