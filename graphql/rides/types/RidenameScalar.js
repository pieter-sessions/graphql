import { Kind, GraphQLScalarType } from "graphql";
import { UserInputError } from "apollo-server-express";

function parseRidename(val) {
  if (val.length > 3) {
    return val;
  }
  throw new UserInputError("invalid ridename");
}

export default new GraphQLScalarType({
  name: "rideName",
  description: "A rideName type",
  parseValue: parseRidename,
  serialize: val => val,
  parseLiteral(ast) {
    if (ast.kind === Kind.STRING) {
      return parseRidename(ast.value);
    }
    throw new UserInputError("invalid ridename");
  }
});
