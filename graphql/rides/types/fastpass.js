import { GraphQLObjectType, GraphQLString } from "graphql";

export default new GraphQLObjectType({
  name: "fastpass",
  description: "a fastpass for a ride",
  fields() {
    return {
      rideId: {
        type: GraphQLString,
        description: "The ID of this specific ride",
        resolve(slot) {
          return slot.rideId;
        }
      },
      timeslot: {
        type: GraphQLString,
        description: "Timeslot for the fastpass",
        resolve(slot) {
          return slot.timeslot;
        }
      }
    };
  }
});
