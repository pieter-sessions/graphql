import { GraphQLObjectType, GraphQLString, GraphQLList } from "graphql";
import FastpassType from "./fastpass";

export default new GraphQLObjectType({
  name: "ride",
  description: "a ride",
  fields() {
    return {
      id: {
        type: GraphQLString,
        description: "ride ID",
        resolve(ride) {
          return ride.id;
        }
      },
      name: {
        type: GraphQLString,
        description: "name of the ride",
        resolve(ride) {
          return ride.name;
        }
      },
      fastpasses: {
        type: new GraphQLList(FastpassType),
        async resolve(ride, args, { fastpassLoader }) {
          throw new Error("fastpass system timed out");
          // return await fastpassLoader.load(ride.id);
        }
      }
    };
  }
});
