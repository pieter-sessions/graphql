import { GraphQLEnumType } from "graphql";

export default new GraphQLEnumType({
  name: "rideCategory",
  values: {
    slowride: { value: "SLOW" },
    coaster: { value: "COASTER" },
    waterride: { value: "WATER" }
  }
});
