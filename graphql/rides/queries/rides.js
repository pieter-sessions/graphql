import { GraphQLList } from "graphql";

import RideModel from "../../../model/Ride";
import Ride from "../types/ride";

export default {
  type: new GraphQLList(Ride),
  async resolve(root, args) {
    console.log(args);
    const rides = await RideModel.find();
    return rides;
  }
};
