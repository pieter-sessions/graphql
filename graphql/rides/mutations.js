import addFastpass from "./mutations/addFastpass";
import addRide from "./mutations/addRide";

export default {
  addFastpass,
  addRide
};
