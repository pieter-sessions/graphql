import Ride from "./model/Ride";
import Fastpass from "./model/Fastpass";

export default async function initData() {
  await Ride.deleteMany({});
  await Fastpass.deleteMany({});

  await Ride.insertMany([
    { id: 0, name: "Big Thunder Mountain" },
    { id: 1, name: "Buzz Lightyear's Space Ranger Spin" },
    { id: 2, name: "Jungle Cruise" },
    { id: 3, name: "It's a small world" },
    { id: 4, name: "Seven Dwarfs Mine Train" },
    { id: 5, name: "Space Mountain" },
    { id: 6, name: "Pirates of the Carribean" },
    { id: 7, name: "Haunted Mansion" },
    { id: 8, name: "Peter Pan's Flight" },
    { id: 9, name: "Enchanted Tiki Room" }
  ]);

  const fastpasses = [...Array(10).keys()].reduce((accumulator, val) => {
    return accumulator.concat([
      { rideId: val, timeslot: "12:00" },
      { rideId: val, timeslot: "13:00" },
      { rideId: val, timeslot: "14:00" },
      { rideId: val, timeslot: "15:00" }
    ]);
  }, []);

  return Fastpass.insertMany(fastpasses);
}
