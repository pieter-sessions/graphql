import mongoose from "mongoose";

const Schema = mongoose.Schema;

// Create the User Schema.
const RideSchema = new Schema({
  id: {
    type: String,
    required: true,
    unique: true
  },
  name: {
    type: String,
    required: true
  },
  type: {
    type: String
  }
});

const Ride = mongoose.model("rides", RideSchema);

export default Ride;
