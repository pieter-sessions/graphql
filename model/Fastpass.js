import mongoose from "mongoose";

const Schema = mongoose.Schema;

// Create the User Schema.
const FastpassSchema = new Schema({
  rideId: {
    type: String,
    required: true
  },
  timeslot: {
    type: String,
    required: true
  }
});

const Fastpass = mongoose.model("fastpasses", FastpassSchema);

export default Fastpass;
