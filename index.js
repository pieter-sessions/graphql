const express = require("express");
const bodyParser = require("body-parser");
const { ApolloServer, PubSub } = require("apollo-server-express");
const schema = require("./graphql/schema").default;
const mongoose = require("mongoose");
const { Logger } = require("mongodb");

import loaders from "./graphql/rides/dataloaders";
const initData = require("./initData").default;
import http from "http";

function setupLogging() {
  let logCount = 0;
  Logger.setCurrentLogger((msg, state) => {
    console.log(`MONGO DB REQUEST ${++logCount}`);
  });
  Logger.setLevel("debug");
  Logger.filter("class", ["Cursor"]);
}

// Initialize the app
const app = express();
const pubSub = new PubSub();

// The GraphQL endpoint
const server = new ApolloServer({
  schema,
  context: () => ({
    fastpassLoader: loaders.fastpassLoader(),
    pubSub
  })
});

server.applyMiddleware({ app });

const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);

httpServer.listen(3000, () => {
  console.log(`🚀 Server ready at http://localhost:3000${server.graphqlPath}`);
  console.log(
    `🚀 Subscriptions ready at ws://localhost:3000${server.subscriptionsPath}`
  );
});

// Connect to MongoDB with Mongoose.
mongoose
  .connect("mongodb://localhost:27017/disney", {
    useCreateIndex: true,
    useNewUrlParser: true
  })
  .then(async () => {
    console.log("MongoDB connected");
    await initData();
    console.log("Data initialised");
    setupLogging();
  })
  .catch(err => console.log(err));
